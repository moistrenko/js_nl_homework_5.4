
/* 
    При выполнении домашнего задания используйте только те конструкции,
    которые есть в уроке.

    TASK 0:

    Напишите функцию sum(c,t), которая возвращает результат суммы c,t

*/

console.log('TASK 0');
function sum (c, t) { 
    return c + t
}
let total = sum(10, 5);
console.log(total);
/* 
    TASK 1:

    Напишите функцию min(a,b), которая возвращает меньшее из чисел a и b

*/
console.log('TASK 1');
function min (a, b) { 
    if (a < b) {
        return a;
    }
    return b;
}
let lessNumber = min(30, 10);
console.log(lessNumber);
/* 
    TASK 2:

    Напишите функцию pow(x,n), которая возвращает x, в степени n

*/
console.log('TASK 2');
function pow(x, n) {
    let result = x;
    for (let i = 1; i < n; i++) {
        result *= x
    }
    return result;
}
let degree = pow(3, 3);
console.log(degree);
/* 
    TASK 3:

    Напишите функцию, которая принимает число,
    и возвращает строку "четное" или "нечетное".

*/
console.log('TASK 3');
function getOddOrEven(number) {
    if (number % 2 == 0) {
        return "четное"
    }
    return "нечетное"
};
let number = getOddOrEven(6);
console.log(number);

/* 
    TASK 4:

    Напишите функцию getColor(23, 100, 134), которая будет принимать три аргумента 
    и возвращать строку вида "rgb(23,100,134)". 
    Если какой-либо из аргументов не задан: например, третий:
    мы вызываем функцию getColor(23,100), в таком случае мы должны
    получить строку "rgb(23,100,0)"

*/
console.log('TASK 4');
function getColor(color1, color2, color3) {
    if (color3) {
    return `rgb(${color1}, ${color2}, ${color3})`;
    } 
    return `rgb(${color1}, ${color2}, 0)`;
}
let color = getColor(120,255,122);
let color2 = getColor(120,255);
console.log(color);
console.log(color2);

// or

function getColor2(color1, color2, color3 = 0) {
    return `rgb(${color1}, ${color2}, ${color3})`;
}

/* 
    TASK 5:

    Напишите 2 функции:

    Первая функция squareNumber(num) должна принимать число, и возвращать квадрат этого числа

    Вторая функция запрашивает у пользователя число от 18 до 50.
    Если пользователь ввел НЕ число, то сделайте ему одно замечание,
    если число, то вызовете функцию squareNumber передав в нее это самое число.
    Необходимо вывести результат пользователю (Либо замечание, либо квадрат числа).  

*/

//Я маленько запутался в условии задачи, не совсем понятно, что требуется: мы даем предупреждение на все НЕ числа? или на всё, что не попадает в диапазон от 18 до 50?
console.log('TASK 5');
function squareNumber(number) {
    return number * number;
}
function getNumber() {  
    let number = prompt('Введите число от 18 до 50', '');
    if (number >= 18 && number <= 50) {
        let correctNumber = Number(number);
        return squareNumber(correctNumber);
    } else {
        alert('Ошибка, введите корректное значение');
    }
}
let square = getNumber();
console.log(square);
